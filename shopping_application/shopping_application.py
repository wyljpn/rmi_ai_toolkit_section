# -*- coding: utf-8 -*-
"""Shpping application

Functions:
1. Add items to the shopping list
2. Remove items from the shopping list
3. Delete items from the shopping list
4. Edit Items in the shopping list
5. Show items added in the list
6. Show items in warehouse

@author: Yulong, Wang
@email: yulong.wang@rakuten.com
@version: 2021/02/02
"""

import os
import sys

from model import Item, ShoppingCart, ItemInventory


def menu():
    print("************** MENU *************")
    print("1. Add items")
    print("2. Remove items")
    print("3. Delete items")
    print("4. Edit items")
    print("5. Show items in shopping cart")
    print("6. Show items in warehouse")
    print("*********************************")


def main():

    inventory = ItemInventory()
    shopping_cart = ShoppingCart()

    while True:
        os.system('cls||clear')
        menu()
        inputed_str = input(
            "Please input the specific number or Q to quit: ").strip()

        if inputed_str == "1":
            add_process(inventory, shopping_cart)
        elif inputed_str == "2":
            remove_process(inventory, shopping_cart)
        elif inputed_str == "3":
            delete_process(inventory, shopping_cart)
        elif inputed_str == "4":
            edit_process(shopping_cart)
        elif inputed_str == "5":
            shopping_cart.show_all_items()
            input("Input any key to continue.")
        elif inputed_str == "6":
            inventory.show_inventory()
            input("Input any key to continue.")
        elif inputed_str.upper().strip() == "Q":
            break
        else:
            print("Please choose a specific number or Q to quit.")


def add_process(inventory, shopping_cart):
    while True:
        os.system('cls||clear')
        shopping_cart.show_all_items()
        inventory.show_inventory()
        inputed_str = input("Input [ID] and [number] to add or Q to quit: ").strip()
        if inputed_str.upper() == "Q":
            break
        else:
            try:
                item_id, number_of_items = inputed_str.split()
                item_id, number_of_items = int(item_id), int(number_of_items)
                item = inventory.getItem(item_id)
                if number_of_items < 0:
                    raise Exception
                if item.num >= number_of_items:
                    # Transaction
                    item_to_cart = Item(item.name, number_of_items)
                    item_to_cart.set_item_id(item_id)
                    shopping_cart.add(item_to_cart)
                    inventory.sell(item_id, number_of_items)
                    input("%d %s was added into shopping cart." %
                          (item_to_cart.num, item_to_cart.name))
                else:
                    input("There are not enough %s(s) in warehouse" % item.name)
                    continue
            except Exception:
                input("Inputed wrong, Input any key back to the last step!")
                continue


def remove_process(inventory, shopping_cart):
    while True:
        os.system('cls||clear')
        shopping_cart.show_all_items()
        inputed_str = input(
            "Input [ID] and [number] to remove or Q to quit: ").strip()
        if inputed_str.upper() == "Q":
            break
        else:
            try:
                item_id, number_of_items = inputed_str.split()
                item_id, number_of_items = int(item_id), int(number_of_items)
                item = shopping_cart.getItem(item_id)
                if number_of_items < 0:
                    raise Exception
                if item.num >= number_of_items:
                    # Transaction
                    item.num -= number_of_items
                    inventory.add(item_id, number_of_items)
                    input("* %d %s(s) are removed from shopping cart." %
                          (number_of_items, item.name))

                    if item.num == 0:
                        shopping_cart.delete(item.ID)
                        input("* %d %s(s) are deleted from shopping cart." %
                              (number_of_items, item.name))
                else:
                    input("You don't have %d %s(s) in your cart!" %
                          (number_of_items, item.name))
            except Exception:
                input("Inputed wrong, Input any key back to the last step!")


def edit_process(shopping_cart):
    while True:
        os.system('cls||clear')
        shopping_cart.show_all_items()
        inputed_str = input(
            "Input [ID] and [remark] to edit or Q to quit: ").strip()
        if inputed_str.upper() == "Q":
            break
        else:
            try:
                item_id, remark = inputed_str.split()
                item_id = int(item_id)
                item = shopping_cart.getItem(item_id)
                old_remark = item.remark
                item.remark = remark
                print("* %s's remark is edited from %s to %s" %
                      (item.name, old_remark, remark))
            except Exception:
                input("Inputed wrong, Input any key back to the last step!")


def delete_process(inventory, shopping_cart):
    os.system('cls||clear')
    while True:
        shopping_cart.show_all_items()
        inputed_str = input(
            "Input [ID] and [number] to delete or Q to quit: ").strip()
        if inputed_str.upper() == "Q":
            break
        else:
            try:
                item_id = int(inputed_str)
                item = shopping_cart.getItem(item_id)
                delete_check = input(
                    "Input 'Y' to delete %s, or input others to quit: " % item.name)
                if delete_check.upper() == "Y":
                    inventory.add(item_id, item.num)
                    shopping_cart.delete(item.ID)
                    print("* %d %s(s) are deleted from shopping cart." %
                          (item.num, item.name))
                else:
                    print("You don't have %s in your cart!" % (item.name))
            except Exception:
                input("Inputed wrong, Input any key back to the last step!")


if __name__ == "__main__":
    main()
