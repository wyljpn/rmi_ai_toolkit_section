
class InventoryInsufficientError(Exception):
    
    def __init__(self, message, status):
        super().__init__(message, status)
        self.message = "Inventory of the item doesn't enough!"
        self.status = status