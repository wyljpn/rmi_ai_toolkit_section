# -*- coding: utf-8 -*-
"""An Object with goods information which can be collected in ShoppingCart or Inventory

Attributes:
    item_id: A int used to specify a item.
    name: A str used to describe an item.
    number_of_items: A int count of the number of an item.
    remark: A str used to store coupon.
"""


class Item:
    item_id = -1
    name = ""
    number_of_items = 0
    remark = ""

    def __init__(self, name, number_of_items):
        self.name = name
        self.number_of_items = number_of_items

    def set_item_id(self, item_id):
        self.item_id = item_id

    def update(self, item):
        """Update the number of the item

        Args:
            item: A Item Object with new num. 
        """
        self.number_of_items = item.number_of_items

    def __str__(self):
        return "%s\t%s\t%d\t%s" % (self.item_id, self.name, self.number_of_items, self.remark)

    @staticmethod
    def item_header():
        print("%s\t%s\t%s\t%s" %
              ("item_id", "Name", "Number of items", "Remark"))


"""An Object with a dict for collecting and manipulating Items.

Attributes:
    itmes_dic: A dict. key:item_id, value: item. It is used to collect Items.
"""


class ShoppingCart:
    itmes_dic = {}

    def __init__(self, items_dic=None):
        if items_dic is not None:
            for item in items_dic.values():
                self.add(item)

    def show_all_items(self):
        print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
        print("Item(s) in your shopping cart:")
        if self.itmes_dic:
            Item.item_header()
            for item in self.itmes_dic.values():
                print(item)
        else:
            print("There is nothing in your shopping cart.")
        print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")

    def getItem(self, item_id):
        return self.itmes_dic[item_id]

    def add(self, item):
        if item.item_id in self.itmes_dic.keys():
            self.itmes_dic[item.item_id].num += item.number_of_items
        else:
            self.itmes_dic[item.item_id] = item

    """Create an item, then add it into items collection (dictionary {item_id: item}) 
    if the item_id doesn't exist in keys of the dictionary. 
    Otherwise, increase the number_of_items of the item to which the item_id maps.
    
    Args:
        item_id: int. It is used to create a new item instance or to find an item in the dictionary.
        number_of_items: int. It is used to create a new item instance 
        or to update the number_of_items of the item to which item_id maps.

    Return:
        None
    """

    def add_item(self, item_id: int, number_of_items: int) -> None:

        if item_id in self.itmes.keys():
            # If the item_id already exists in the dictionary's keys, increase item.number_of_items by adding it to number_of_items.
            self.itmes[item_id].number_of_items += number_of_items
        else:
            # Create an item, then add it into the dictionary.
            self.itmes[item_id] = Item(item_id, number_of_items)

        return


    def delete(self, item_id):
        return self.itmes_dic.pop(item_id)

    def remove(self, item_id, number_of_items):
        pass


"""An Object used to manage inventory of Items.
"""


class ItemInventory:
    available_items = {}

    item_id = 1

    def __init__(self):
        """Add some Item Objects to the inventory for testing.
        """
        item_list = []
        item_list.append(Item("Apple", 10))
        item_list.append(Item("Orange", 10))
        item_list.append(Item("Banana", 10))
        item_list.append(Item("Pear", 10))

        for item in item_list:
            item.set_item_id(self.item_id)
            self.available_items[self.item_id] = item
            self.item_id += 1

    def show_inventory(self):
        print("---------------------------------------")
        print("Item(s) in warehouse:")
        if self.available_items:
            Item.item_header()
            for item in self.available_items.values():
                print(item)
        else:
            print("There is nothing in warehouse.")
        print("---------------------------------------")

    def sell(self, item_id, sell_num):
        """Take away an item from inventory

        Args:
            item_ID: an ID of an Item Object.
            sell_num: a num of an Item Object needed to take away from inventory.
        """
        ori_item = self.available_items[item_id]
        ori_item.num -= sell_num
        return

    def getItem(self, item_ID):
        return self.available_items[item_ID]

    def add(self, ID, num):
        item = self.available_items[ID]
        item.num += num

    def remove(self):
        pass

    def delete(self):
        pass
